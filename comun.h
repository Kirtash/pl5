/*----------------------- comun.h -----------------------------*/

/* fichero con definciones comunes para los ficheros .l y .y */
#include <vector>

typedef struct {
   char *lexema;
   int nlin,ncol;
   int tipo;
   int variable;
   int valor;
   int dir;
   int numElem;
   int numSalto;
   bool esArray;
   bool esId;
   string trad, traddecl, salto, cod;
   //int fila, columna;
   //String lexema;
} MITIPO;

//Importante, tipo definido ha de ser YYSTYPE.
#define YYSTYPE MITIPO

#define ERRLEXICO           1
#define ERRSINT             2
#define ERREOF              3
#define ERRLEXEOF           4

#define ERR_YADECL          5
#define ERR_NODECL          6
#define ERR_MAXVAR          7
#define ERR_NOCABE          8
#define ERR_MAXTEMP         9
#define ERR_RANGO           10
#define ERR_IFWHILE         11
#define ERR_CHR             12
#define ERR_ORD             13
#define ERR_TRUNC           14
#define ERR_FALTAN          15
#define ERR_SOBRAN          16
#define ERR_INDICE_ENTERO   17
#define ERR_ASIG            18
#define ERR_OPIZQ           19
#define ERR_OPDER           20

//Mis definiciones:
#define VARIABLE        1
#define ARRAY           2
#define FUNCION         3

#define ENTERO          1
#define REAL            2
#define LOGICO          3
#define CHAR            4
//#define NUMERICO      5
#define NUMERICO        0

//int MAX_VARIABLES = 1000;
//int MAX_AMBITOS   = 100;
//int MAX_FUNCIONES = 30;

class Simbolo{
	public:
  	string nombre;
  	int tipo;
  	int dir;
  	int tam;

  	Simbolo(){
      this->nombre = "";
      this->tipo  =
  		this->dir   =
  		this->tam   = 0;
    }

  	Simbolo(string nombre, int tipo, int dir, int tam){
      for (int i=0; i<nombre.length(); i++)
          nombre[i] = tolower(nombre[i]);
  		this->nombre = nombre;
  		this->tipo = tipo;
  		this->dir = dir;
  		this->tam = tam;
  	}

    void verSimbolo() {
      cout<< "nombre: " << this->nombre << endl;
      cout<< "tipo: " << this->tipo << endl;
      cout<< "dir: " << this->dir << endl;
      cout<< "tam: " << this->tam << endl;
    }
};

class TablaSimbolos{
	private:
		vector<Simbolo*> tabla;
		int dir;

	public:
		TablaSimbolos(){
			this->dir = 0;
		}

		int dirActual(){
			return this->dir;
		}

		void nuevoSimbolo(string nombre, int tipo, int dir, int tam){
      for (int i=0; i<nombre.length(); i++)
           nombre[i] = tolower(nombre[i]);
			this->tabla.push_back(new Simbolo(nombre, tipo, dir, tam));
			this->dir += tam;
		};

    bool existeID(string id){
      for (int i=0; i<id.length(); i++)
           id[i] = tolower(id[i]);
      for(int i=0; i < this->tabla.size(); i++){
				if(this->tabla[i]->nombre == id)
					return true;
			}
      return false;
    }

		Simbolo* buscarSimbolo(string id){
      for (int i=0; i<id.length(); i++)
           id[i] = tolower(id[i]);
			for(int i=0; i < this->tabla.size(); i++){
				if(this->tabla[i]->nombre == id)
					return this->tabla[i];
			}
			return NULL;
		}

		Simbolo* buscarPorDir(int direc){
			for(int i=0; i < this->tabla.size(); i++){
				if(this->tabla[i]->dir == direc)
					return this->tabla[i];
			}
			return NULL;
		}

    void verTablaSimbolos() {
      for(int i=0; i < this->tabla.size(); i++){
        cout<< "\n-- Simbolo: " << i << " --" << endl;
				this->tabla[i]->verSimbolo();
			}
    }
};

class Tipo {
	public:
		int tipo;    //Número del tipo.
    int tBase;   //Tipo base de ese Array.
		int tam;     //Tamaño.
    int tamTotal;//Tamaño total del Array.
		int ini;     //Inicio Array.
    int fin;     //Fin Array.

  Tipo() {
    tipo = tBase = tam = ini = fin = 0;
  }

	Tipo(int tipo, int tBase, int tam, int tamTotal, int ini, int fin){
		this->tipo = tipo;
    this->tBase = tBase;
		this->tam = tam;
    this->tamTotal = tamTotal;
    this->ini = ini;
    this->fin = fin;
	}

  void verTipo() {
    cout<< "tipo: " << this->tipo << endl;
    cout<< "tBase: " << this->tBase << endl;
    cout<< "tam: " << this->tam << endl;
    cout<< "tamTotal: " << this->tamTotal << endl;
    cout<< "ini: " << this->ini << endl;
    cout<< "fin: " << this->fin << endl;
  }
};

class TablaTipos{
	private:
		vector<Tipo*> tabla;

	public:
		TablaTipos(){
			//nuevoTipo(1,1,1,1);
      //cout << "Añadiendo primero:\n";
      this->tabla.push_back(new Tipo(1,1,1,1,1,1));
      //cout << "Añadiendo segundo:\n";
			nuevoTipo(2,1,2,2);
      nuevoTipo(3,1,3,3);
			nuevoTipo(4,1,4,4);
		}

		int nuevoId(){
			return this->tabla.size() + 1;
		}
		void nuevoTipo(int tipo, int tBase, int ini, int fin){
      //                                              tam   |    (tam    *     tamTotal de tBase)
			this->tabla.push_back(new Tipo(tipo, tBase, fin-ini+1, (fin-ini+1) * this->tabla[tBase-1]->tamTotal, ini, fin));
		}
    int size(){
      return this->tabla.size();
    }
    Tipo* getTipo(int tipo){
      for(int i=0; i < this->tabla.size(); i++){
        if(this->tabla[i]->tipo == tipo)
          return this->tabla[i];
      }
      return new Tipo();
    }
    int numIndices(int tipo) {
      if (tipo > 4)
        return 1 + numIndices(this->tabla[tipo-1]->tBase);
      else
        return 0;
    }
    int tBaseEsencial(int tipo){
      if (tipo > 4) {
        //verTablaTipos();
        //cout << "tipo: " << tipo << endl;
        //cout << "tBase: " << this->tabla[tipo-1]->tBase << endl;
        return tBaseEsencial(this->tabla[tipo-1]->tBase);
      }
      else
          return tipo;
    }

    int tBase(int tipo){
      for(int i = 0; i < this->tabla.size(); i++){
        if(this->tabla[i]->tipo == tipo)
          return this->tabla[i]->tBase;
      }
      return -1;
    }
		int tam(int tipo){
			for(int i = 0; i < this->tabla.size(); i++){
				if(this->tabla[i]->tipo == tipo)
					return this->tabla[i]->tam;
			}
      return -1;
		}
    // int calculaTamTotal(int tipo){
    //   for(int i = 5; i < this->tabla.size(); i++){
    //     if(this->tabla[i]->tipo == tipo)
    //       return this->tabla[i]->tamTotal;
    //   }
    //   return 1;
    // }
    int tamTotal(int tipo){
      for(int i = 0; i < this->tabla.size(); i++){
        if(this->tabla[i]->tipo == tipo)
          return this->tabla[i]->tamTotal;
      }
      return -1;
    }
    int ini(int tipo){
			for(int i = 0; i < this->tabla.size(); i++){
				if(this->tabla[i]->tipo == tipo)
					return this->tabla[i]->ini;
			}
      return -1;
		}
    int fin(int tipo){
      for(int i = 0; i < this->tabla.size(); i++){
        if(this->tabla[i]->tipo == tipo)
          return this->tabla[i]->fin;
      }
      return -1;
    }
    void verTablaTipos() {
      cout << "Imprimiendo tabla de tipos\n";
      for(int i=0; i < this->tabla.size(); i++){
        cout<< "\n-- Tipo: " << i << " --" << endl;
        this->tabla[i]->verTipo();
      }
    }
};

int ctebool(string s);
string relop(string s);
string minusculas(string s);
void addRef(char *s);
bool existeSimboloAux(string id);
void addSimbolo(char *s, int nlin, int ncol);
void msgError(int nerror,int nlin,int ncol,const char *s);
void msgErrorOperador(int tipoesp,const char *op,int linea,int columna,int lado);
void msgErrorSemantico(int nerror,int nlin,int ncol,const char *s);
