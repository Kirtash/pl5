/*------------------------------ plp5.y -------------------------------*/
%token _program _begin _end _var _boolean _integer _real _char _wri _read _if _then _else _while _do _ybool _obool
%token _ctebool _nobool _chr _ord _trunc _array _of _id _nint _nfix _ctechar _coma _pyc _punto _dosp _lpar _rpar
%token _relop _addop _mulop _assop _lcor _rcor _ptopto

%{

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <string.h>
#include <vector>


using namespace std;

#include "comun.h"

// variables y funciones del A. Léxico
extern int ncol,nlin,findefichero;

//Llamada a léxico.
extern int yylex();
//Llamada a lexema.
extern char *yytext;
//Llamada a fichero.
extern FILE *yyin;

int yyerror(char *s);

/////////////////
// ESTRUCTURAS //
//////////////////////////////////////
//SIMBOLOS                          //
  Simbolo* simboloAux;              //
  TablaSimbolos tsimbolos;          //
  vector<string> lsimbolos;         //
  vector<string> lref;              //
//TIPOS                             //
  Tipo tipoAux;                     //
  TablaTipos ttipos;                //
//POSICIONES                        //
  int posDirProg;                   //
  int posDirTemp;                   //
  int posTabTipos;                  //
  int numSalto;                     //
//////////////////////////////////////

//.trad
//.tipo
//.temp

void verSimbolo(MITIPO a) {
  cout<< "__VER SIMBOLO__ : " << a.lexema << endl;
  cout<< "lexema: " << a.lexema << endl;
  cout<< "nlin: " << a.nlin << endl;
  cout<< "ncol: " << a.ncol << endl;
  cout<< "tipo: " << a.tipo << endl;
  cout<< "dir: " << a.dir << endl;
  cout<< "variable: " << a.variable << endl;
  cout<< "esArray: " << a.esArray << endl;
  cout<< "esId: " << a.esId << endl;
  cout<< "trad: " << a.trad << endl;
  cout<< "traddecl: " << a.traddecl << endl;
}

void inicializacion() {
  //TODO Introducir tipos básicos en la tabla de tipos.
  posDirProg  = 0;
  posDirTemp  = 15000;
  posTabTipos = 5; //De la posición 1 a la 4 están ocupadas por los tipos básicos.
  numSalto    = 0;
}

string variable(MITIPO a) {
    string t;

    if (a.esId)
        if (a.tipo == ENTERO)
            t = a.trad + ".intValue()";
        else
            t = a.trad + ".doubleValue()";
    else
        t = a.trad;

    return t;
}

MITIPO operacion(MITIPO izq, MITIPO op, MITIPO der) {
    MITIPO a;
    string ti, td;
    a.traddecl = "";
    a.trad     = "ERROR";
    a.esId     = false; //Si era ID, deja de serlo

    if(izq.tipo != der.tipo) {    //IZQ  !=  DER ->   ENTERO-REAL   /   REAL-ENTERO
        a.tipo = REAL;

        if (izq.tipo == ENTERO) { //ENTERO - REAL
            if(izq.esId)
                ti = izq.trad + ".toDouble()";
            else
                ti = "(new Integer(" + izq.trad + ")).toDouble()";
            //if (der.esId)
            //    td = variable(der);
            //else
            //    td = der.trad;
            td = variable(der);
        } else {                  //REAL - ENTERO
            if(der.esId)
                td = der.trad + ".toDouble()";
            else
                td = "(new Integer(" + der.trad + ")).toDouble()";
            //if (izq.esId)
            //    ti = variable(izq);
            //else
            //    ti = izq.trad;
            ti = variable(izq);
        }
    } else {                      //IZQ  !=  DER
        if (izq.tipo == REAL)
            a.tipo = REAL;
        else
            a.tipo = ENTERO;


        if (izq.esId)
            ti = variable(izq);
        else
            ti = izq.trad;

        if (der.esId)
            td = variable(der);
        else
            td = der.trad;
    }

    a.trad = ti + op.lexema + td;
    //System.out.println("La concatenación de atributos es igual a: " + trad);
    return a;
}

bool esArray(int tipo){
	return tipo > 4;
}

int NuevaTemporal() {
  if(posDirTemp >= 16383)
    msgError(ERR_MAXTEMP, nlin, ncol, "");
  return posDirTemp++;
}

int NuevaDirProg(int nlin, int ncol, string var) {
  if(posDirProg >= 11000)
    msgError(ERR_NOCABE, nlin, ncol, var.c_str());
  return posDirProg++;
}

int NuevaDirProg(int tam, int nlin, int ncol, string var) {
  //posDirProg += tam;
  if((posDirProg + tam) >= 11000)
    msgError(ERR_NOCABE, nlin, ncol, var.c_str());
  int posDirProgAux = posDirProg;
  posDirProg += tam;
  return posDirProgAux;
}

%}

%%

//S         : program id pyc BDecl Bloque punto
S           :   _program _id _pyc BDecl
                //{
                //    tsimbolos.verTablaSimbolos();
                //    ttipos.verTablaTipos();
                //}
                Bloque _punto {
                    //printf("%s", $$.cod.c_str());
                    int tk = yylex();
                    if (tk == 0) {
                      $$.cod = $4.cod + $5.cod + "halt\n";
                      printf("%s", $$.cod.c_str());
                    } else /* comprobar que después del programa no hay ningún token más */
                      yyerror((char *)"");
                }
            ;

//Bloque    : begin SeqInstxr end
Bloque      : _begin SeqInstr _end {$$.cod = $2.cod;}
            ;

//BDecl     : BlVar BDecl
//BDecl     : e
BDecl       : BlVar BDecl {$$.cod = $1.cod + $2.cod;}
            | {$$.cod = "";}
            ;

//BlVar     : var Decl pyc
BlVar       : _var Decl _pyc {$$.cod = $2.cod;}
            ;

//Decl      : Decl pyc DVar
//Decl      : DVar
Decl        : Decl _pyc DVar {$$.cod = $1.cod + $3.cod;}
            | DVar {$$.cod = $1.cod;}
            ;

//DVar      : LIdent dosp Tipo
DVar        : LIdent _dosp Tipo {
                ostringstream oss;
                //Mirar que no nos pasamos de las posiciones para variables NO temporales.
                if(posDirProg >= 15000)
                    msgError(ERR_NOCABE, $2.nlin, $2.ncol, $2.lexema);

                //TODO If (dirección actual + tamaño del tipo >= 15000) error
                //Else actualizamos dirección actual y seguimos.
                int tipoEsencial = ttipos.tBaseEsencial($3.tipo);
                if(esArray($3.tipo)){
                    //Introducimos en tabla de símbolos los elementos de lsimbolos.
                    for (int i=0; i<lsimbolos.size(); ++i){
                        int tamTotal = ttipos.tamTotal($3.tipo);
                        int tmp = NuevaDirProg(tamTotal, $2.nlin, $2.ncol, lsimbolos[i]);
                        //Introducimos en tabla de símbolos.
                        tsimbolos.nuevoSimbolo(lsimbolos[i], $3.tipo, tmp, tamTotal);
                        //Inicialización del array.
                        for (int j = 0; j < tamTotal; ++j) {
                            if (tipoEsencial == REAL)
                                oss << "mov $0 " << tmp+j <<";LIdent dosp Tipo("<< lsimbolos[i] <<")\n";
                            else
                                oss << "mov #0 " << tmp+j <<";LIdent dosp Tipo("<< lsimbolos[i] <<")\n";
                        }
                    }
                } else {
                    for (int i=0; i<lsimbolos.size(); ++i){
                        int tmp = NuevaDirProg($2.nlin, $2.ncol, lsimbolos[i]);
                        //Introducimos en tabla de símbolos.
                        tsimbolos.nuevoSimbolo(lsimbolos[i], $3.tipo, tmp, 1);
                        if (tipoEsencial == REAL)
                            oss << "mov $0 " << tmp <<";LIdent dosp Tipo("<< lsimbolos[i] <<")\n";
                        else
                            oss << "mov #0 " << tmp <<";LIdent dosp Tipo("<< lsimbolos[i] <<")\n";
                    }
                }
                //$$.tipo = $3.tipo;
                //$$.dir = tmp;
                //Borramos de los elementos de las listas auxiliares.
                lsimbolos.clear();
                $$.cod = oss.str();
              }
            ;

//TipoSimple: integer
//TipoSimple: real
//TipoSimple: char
//TipoSimple: boolean
TipoSimple  : _integer {$$.tipo = ENTERO;}
            | _real    {$$.tipo = REAL;}
            | _char    {$$.tipo = CHAR;}
            | _boolean {$$.tipo = LOGICO;}
            ;

//Tipo      : TipoSimple
//Tipo      : array lcor nint ptopto nint rcor of Tipo
Tipo        : TipoSimple {
                //No es Array.
                $$.tipo = $1.tipo;
                //$$.cod  = $1.cod;
                //$$.dir  = $1.dir;
              }
            | _array _lcor _nint _ptopto _nint {
                if (atoi($3.lexema) > atoi($5.lexema))
                    msgError(ERR_RANGO, $5.nlin, $5.ncol, $5.lexema);
              } _rcor _of Tipo {
                //Añadir a tabla de tipos:
                $$.tipo = ttipos.size()+1; //Aquí ponemos la posición que ocupa el tipo en la tabla de tipos.
                ttipos.nuevoTipo($$.tipo, $9.tipo, atoi($3.lexema), atoi($5.lexema));
                //$$.cod  = $9.cod;  //Código de la declaración
                //$$.dir  = $9.dir;  //Dirección de inicio del tipo?? en la lista de variables.

              }
            ;

//LIdent    : LIdent coma id
//LIdent    : id
LIdent      : LIdent _coma _id {addSimbolo($3.lexema, $3.nlin, $3.ncol);}
            | _id {addSimbolo($1.lexema, $1.nlin, $1.ncol);}
            ;

//SeqInstr  : SeqInstr pyc Instr
//SeqInstr  : Instr
            // Aquí tenemos que almacenar el valor de ctemp para después del punto y coma recuperarlo.
            // ctemp = 15000 + "desplazamiento".
            // [0 - 14999]    -> Variables del programa.
            // [15000 - 16383]-> Variables temporales para operar.
//SeqInstr  : {$$.temp = ctemp;} SeqInstr _pyc {ctemp = $$.temp;} Instr
SeqInstr    : SeqInstr _pyc {$$.dir = posDirTemp;} Instr {$$.cod = $1.cod + $4.cod; posDirTemp = $3.dir;}
            | {$$.dir=posDirTemp;} Instr {$$.cod = $2.cod; posDirTemp=$1.dir;}
            ;

//Instr     : Bloque
//Instr     : Ref assop Expr
//Instr     : wri lpar LExpr rpar
//Instr     : read lpar LRef rpar
//Instr     : if Expr then Instr
//Instr     : if Expr then Instr else Instr
//Instr     : while Expr do Instr
// Instr       : Bloque
//             | Ref _assop Expr
//             | _wri _lpar LExpr _rpar
//             | _read _lpar LRef _rpar
//             ;
//             | _if Expr _then Instr
//             | _if Expr _then Instr _else Instr
//             | _while Expr _do Instr
Instr       : Bloque {$$.cod = $1.cod;}
            | Ref _assop Expr {
                  ostringstream oss;
                  oss << $3.cod;
                  //int tmp = NuevaTemporal();
                  //$$.dir = tmp;
                  if($1.tipo == ENTERO && $3.tipo == ENTERO) {
                      oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                      $$.cod = oss.str();
                      $$.tipo = ENTERO;
                  } else if($1.tipo == REAL && $3.tipo == ENTERO) {
                      oss << "mov " << $3.dir << " A;Ref assop Expr\n";
                      oss << "itor" << '\n';
                      oss << "mov A " << $1.dir << '\n';
                      $$.cod = oss.str();
                      $$.tipo = REAL;
                  } else if($1.tipo == REAL && $3.tipo == REAL) {
                      oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                      $$.cod = oss.str();
                      $$.tipo = REAL;
                  } else if($1.tipo == CHAR && $3.tipo == CHAR) {
                      oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                      $$.cod = oss.str();
                      $$.tipo = CHAR;
                  } else if($1.tipo == LOGICO && $3.tipo == LOGICO) {
                      oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                      $$.cod = oss.str();
                      $$.tipo = LOGICO;
                  } else {
                      //Puede ser array.
                      int tipoEsencial = ttipos.tBaseEsencial($1.tipo);
                      if(tipoEsencial == ENTERO && $3.tipo == ENTERO) {
                          oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                          $$.cod = oss.str();
                          $$.tipo = ENTERO;
                      } else if(tipoEsencial == REAL && $3.tipo == ENTERO) {
                          oss << "mov " << $3.dir << " A;Ref assop Expr\n";
                          oss << "itor" << '\n';
                          oss << "mov A " << $1.dir << '\n';
                          $$.cod = oss.str();
                          $$.tipo = REAL;
                      } else if(tipoEsencial == REAL && $3.tipo == REAL) {
                          oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                          $$.cod = oss.str();
                          $$.tipo = REAL;
                      } else if(tipoEsencial == CHAR && $3.tipo == CHAR) {
                          oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                          $$.cod = oss.str();
                          $$.tipo = CHAR;
                      } else if(tipoEsencial == LOGICO && $3.tipo == LOGICO) {
                          oss << "mov " << $3.dir << ' ' << $1.dir << ";Ref assop Expr\n";
                          $$.cod = oss.str();
                          $$.tipo = LOGICO;
                      }
                      //No es válido
                      else
                        msgError(ERR_ASIG, $2.nlin, $2.ncol, $2.lexema);
                  }
              }
            | _wri _lpar LExpr _rpar {
                  ostringstream oss;
                  //int tmp = NuevaTemporal();
                  oss << $3.cod;
                  for (int i=0; i<lref.size(); ++i){
                      simboloAux = tsimbolos.buscarSimbolo(lref[i]);
                      //Error no declarado -> No es necesario, ya comprobamos si existe al añadir a lref.
                      //if (simboloAux == NULL)
                      //    msgError(ERR_NODECL, $3.nlin, $3.ncol, $3.lexema);
                      if (simboloAux->tipo == ENTERO)
                          oss << "wri " << simboloAux->dir <<";wri lpar LExpr(entero) rpar\n";
                      else if (simboloAux->tipo == REAL)
                          oss << "wrr " << simboloAux->dir <<";wrr lpar LExpr(real) rpar\n";
                      else if (simboloAux->tipo == CHAR)
                          oss << "wrc " << simboloAux->dir <<";wrc lpar LExpr(caracter) rpar\n";
                      else if (simboloAux->tipo == LOGICO) {
                          //Importante valor de simboloAux.dir
                          //simboloAux.dir
                          //Incrementamos valor de numSalto.
                          int numSalto1 = ++numSalto; //L1
                          $$.numSalto = ++numSalto;   //L2
                          /*Acción semántica que comprueba si el tipo devuelto por Expr es de tipo booleano o no*/
                          oss << ";wrc lpar LExpr(logico) rpar\n";
                          oss << "mov " << simboloAux->dir << " A\n"; //Carga variable
                          oss << "jz L" << numSalto1 << '\n';         //jz L1
                          oss << "wrc #116\n";                        //true
                          oss << "jmp L" << $$.numSalto << '\n';      //jmp L2
                          oss << "L" << numSalto1 << '\n';            //L1
                          oss << "wrc #102\n";                        //false
                          oss << "L" << $$.numSalto << '\n';          //L2
                      } else //Error tipo no válido.
                          cerr << "ERROR: Intentando imprimir tipo NO VÁLIDO.\n";
                  }
                  if (minusculas($1.lexema) == "writeln")
                    oss << "wrl\n";
                  $$.cod = oss.str();
                  lref.clear();
                }
            | _read _lpar LRef _rpar {
                  ostringstream oss;
                  //int tmp = NuevaTemporal();
                  oss << $3.cod;
                  for (int i=0; i<lref.size(); ++i){
                      simboloAux = tsimbolos.buscarSimbolo(lref[i]);
                      if (simboloAux->tipo == ENTERO)
                          oss << "rdi " << simboloAux->dir <<";rdi lpar LRef rpar\n";
                      else if (simboloAux->tipo == REAL)
                          oss << "rdr " << simboloAux->dir <<";rdr lpar LRef rpar\n";
                      else if (simboloAux->tipo == CHAR)
                          oss << "rdc " << simboloAux->dir <<";rdc lpar LRef rpar\n";
                      else if (simboloAux->tipo == LOGICO) {
                          //oss << "rdc " << simboloAux->dir <<";rdc lpar LRef rpar\n";
                          //Incrementamos valor de numSalto.
                          int numSalto1 = ++numSalto; //L1
                          $$.numSalto = ++numSalto;   //L2
                          /*Acción semántica que comprueba si el tipo devuelto por Expr es de tipo booleano o no*/
                          oss << ";wrc lpar LExpr(logico) rpar\n";
                          oss << "rdc " << simboloAux->dir <<";rdc lpar LRef rpar\n";
                          oss << "mov #116 A\n";                      //Carga variable
                          oss << "eqli " << simboloAux->dir << '\n';  //Si son iguales (Si se recibe 't')
                          oss << "jz L" << numSalto1 << '\n';         //jz L1
                          oss << "mov #1 "<< simboloAux->dir << '\n'; //Si es true, cargamos 1 en bool
                          oss << "jmp L" << $$.numSalto << '\n';      //jmp L2
                          oss << "L" << numSalto1 << '\n';            //L1
                          oss << "mov #0 "<< simboloAux->dir << '\n'; //Si es false, cargamos 0 en bool
                          oss << "L" << $$.numSalto << '\n';          //L2
                      } else {
                          //TODO Implementar para Arrays?
                          //Error tipo no válido.
                          msgError(ERR_ORD, $3.nlin, $3.ncol, $3.lexema);
                      }
                  }
                  lref.clear();
                  $$.cod = oss.str();
              }
            | _if Expr {
                    if ($2.tipo != LOGICO)
                      msgError(ERR_IFWHILE, $1.nlin, $1.ncol, $1.lexema);
                    //Incrementamos valor de numSalto.
                    $$.numSalto = ++numSalto;
                    ostringstream oss;
                    /*Acción semántica que comprueba si el tipo devuelto por Expr es de tipo booleano o no*/
                    oss << $2.cod << ";if Expr then Instr Instrp\n";
                    oss << "mov " << $2.dir << " A\n";
                    oss << "jz L" << numSalto << '\n';
                    $$.cod = oss.str();
                } _then Instr {
                    ostringstream oss;
                    oss << $3.cod;
                    oss << $5.cod;
                    //oss << "L" << $3.numSalto << '\n';
                    $$.cod = oss.str();
                } Instrp {
                    ostringstream oss;
                    oss << $6.cod;
                    oss << $7.salto;
                    oss << "L" << $3.numSalto << '\n';
                    oss << $7.cod;
                    $$.cod = oss.str();
                }
            | _while Expr {
                    if ($2.tipo != LOGICO)
                      msgError(ERR_IFWHILE, $1.nlin, $1.ncol, $1.lexema);
                }_do Instr {
                    ostringstream oss;
                    //Incrementamos valor de numSalto.
                    int numSalto1 = ++numSalto; //L1
                    $$.numSalto = ++numSalto;   //L2
                    /*Acción semántica que comprueba si el tipo devuelto por Expr es de tipo booleano o no*/
                    oss << "L" << $$.numSalto;                  //L2:
                    oss << ";while Expr do Instr\n";            //;while Expr do Instr
                    oss << $2.cod;                              //Expr.cod
                    oss << "mov " << $2.dir << " A\n";          //mov Expr.dir A
                    oss << "jz L" << numSalto1 << '\n';         //jz L1
                    oss << $5.cod;                              //Instr.cod
                    oss << "jmp L" << $$.numSalto << '\n';      //jmp L2
                    oss << "L" << numSalto1 << '\n';            //L1:
                    $$.cod = oss.str();
                }
                ;

Instrp      : _else Instr {
                    $$.numSalto = ++numSalto;
                    ostringstream oss, oss2;
                    //Trozo de salto incondicional del if (si es true)
                    oss2 << "jmp L" << numSalto << '\n';
                    $$.salto = oss2.str();
                    //Trozo de código del else (si es false)
                    oss << $2.cod;
                    //Etiqueta de destino para salto incondicional del if (si es true)
                    oss << "L" << numSalto << '\n';
                    $$.cod = oss.str();
                }
            | {$$.cod = $$.salto = "";}
            ;

//LExpr     : LExpr coma Expr
//LExpr     : Expr
LExpr       : LExpr _coma Expr {
                  addRef($3.lexema);
                  $$.cod = $1.cod + $3.cod;
                }
            | Expr {
                  addRef($1.lexema);
                  $$ = $1;
                }
            ;

//LRef      : LRef coma Ref
//LRef      : Ref
LRef        : LRef _coma Ref {
                  addRef($3.lexema);
                  $$.cod = $1.cod + $3.cod;
                }
            | Ref {
                  addRef($1.lexema);
                  $$ = $1;
                }
            ;

//Expr      : Esimple relop Esimple
//Expr      : Esimple
Expr        : Esimple _relop Esimple {
                  int tmp = NuevaTemporal();
        					//if($1.tipo == LOGICO)
                  //  msgError(ERR_OPIZQ, $2.nlin, $2.ncol, $2.lexema);

                  ostringstream oss;
                  oss << $1.cod << $3.cod;
                  if($1.tipo == ENTERO && $3.tipo == ENTERO){
                    //$$.tipo = ENTERO;
                    oss << "mov " << $1.dir << " A;Esimple relop Esimple\n";
                    oss << relop($2.lexema) << "i " << $3.dir << '\n';
                    oss << "mov A " << tmp << '\n';
        					}
        					else if($1.tipo == REAL && $3.tipo == ENTERO){
                    //$$.tipo = REAL;
                    oss << "mov " << $3.dir << " A;Esimple relop Esimple\n";
                    oss << "itor" << '\n';
                    oss << "mov A " << tmp << '\n';
                    oss << "mov " << $1.dir << " A\n";
                    oss << relop($2.lexema) << "r " << tmp << '\n';
                    oss << "mov A " << tmp << '\n';
        					}
        					else if($1.tipo == ENTERO && $3.tipo == REAL){
                    //$$.tipo = REAL;
                    oss << "mov " << $1.dir << " A;Esimple(nint) relop Esimple\n";
                    oss << "itor" << '\n';
                    oss << relop($2.lexema) << "r " << $3.dir << '\n';
                    oss << "mov A " << tmp << '\n';
        					}
        					else if($1.tipo == REAL && $3.tipo == REAL){
                    //$$.tipo = REAL;
                    oss << "mov " << $1.dir << " A;Esimple relop Esimple(nint)\n";
                    oss << relop($2.lexema) << "r " << $3.dir << '\n';
                    oss << "mov A " << tmp << '\n';
        					}
                  else if($1.tipo == CHAR && $3.tipo == CHAR){
                    //$$.tipo = CHAR;
                    oss << "mov " << $1.dir << " A;Esimple relop Esimple\n";
                    oss << relop($2.lexema) << "i " << $3.dir << '\n';
                    oss << "mov A " << tmp << '\n';
        					}
        					else
                    msgErrorOperador($1.tipo, $2.lexema, $2.nlin, $2.ncol, ERR_OPDER);
                    //msgError(ERR_OPDER, nlin, ncol, $2.lexema);
                  $$.tipo = LOGICO;
                  $$.dir = tmp;
                  $$.cod = oss.str();
      				}
            | Esimple { $$ = $1;}
            ;

//Esimple   : Esimple addop Term
//Esimple   : Esimple obool Term
//Esimple   : Term
//Esimple   : addop Term
Esimple     : Esimple _addop Term {
                  if ($1.tipo != ENTERO && $1.tipo != REAL)
                      msgErrorOperador(NUMERICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPIZQ);
                      //msgError(ERR_OPIZQ, $2.nlin, $2.ncol, $2.lexema);
                  if ($3.tipo != ENTERO && $3.tipo != REAL)
                      msgErrorOperador(NUMERICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPDER);
                      //msgError(ERR_OPDER, $2.nlin, $2.ncol, $2.lexema);

                  ostringstream oss;
                  oss << $1.cod << $3.cod;
                  int tmp = NuevaTemporal();
                  $$.dir = tmp;
                  if ($1.tipo == ENTERO && $3.tipo == ENTERO) {
                      $$.tipo = ENTERO;
                      oss << "mov "  << $1.dir << " A;Esimple(nint) addop(" << $2.lexema << ") Term(nint)\n";
                      if (string($2.lexema) == "-")
                          oss << "subi " << $3.dir << '\n';
                      else
                          oss << "addi " << $3.dir << '\n';
                      oss << "mov A "<< tmp << '\n';
                  } else if ($1.tipo == REAL && $3.tipo == REAL) {
                      $$.tipo = REAL;
                      oss << "mov "  << $1.dir << " A;Esimple(nfix) addop Term(nfix)\n";
                      if (string($2.lexema) == "-")
                          oss << "subr " << $3.dir << '\n';
                      else
                          oss << "addr " << $3.dir << '\n';
                      oss << "mov A "<< tmp << '\n';
                  } else { //MIX real y entero
                      $$.tipo = REAL;
                      if ($1.tipo == ENTERO) { //$1.tipo == ENTERO && $3.tipo == REAL
                          oss << "mov "  << $1.dir << " A;Esimple(nint) addop Term(nfix)\n";
                          oss << "itor"  << '\n';
                          if (string($2.lexema) == "-")
                              oss << "subr " << $3.dir << '\n';
                          else
                              oss << "addr " << $3.dir << '\n';
                          oss << "mov A "<< tmp << '\n';
                      } else {                //$1.tipo == REAL && $3.tipo == ENTERO
                          oss << "mov "  << $3.dir << " A;Esimple(nfix) addop Term(nint)\n";
                          oss << "itor"  << '\n';
                          if (string($2.lexema) == "-") {
                              oss << "mov A "<< tmp << '\n';
                              oss << "mov "  << $1.dir << " A\n";
                              oss << "subr " << tmp << '\n';
                          } else
                              oss << "addr " << $1.dir << '\n';
                          oss << "mov A "<< tmp << '\n';
                      }

                  }
                  $$.cod = oss.str();
              }
            | Esimple _obool {
                    if ($1.tipo != LOGICO) //msgError(ERR_OPIZQ, $2.nlin, $2.ncol, $2.lexema);
                        msgErrorOperador(LOGICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPIZQ);
                } Term {
                    if ($4.tipo != LOGICO) //msgError(ERR_OPDER, $2.nlin, $2.ncol, $2.lexema);
                        msgErrorOperador(LOGICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPDER);

                    int tmp = NuevaTemporal();
                    ostringstream oss;
                    oss << $1.cod  << $4.cod;
                    oss << "mov "  << $1.dir << " A;Term(bool) obool Factor(bool)\n";
                    oss << "ori "  << $4.dir << '\n';
                    oss << "mov A "<< tmp << '\n';
                    $$.tipo = LOGICO;
                    $$.dir = tmp;
                    $$.cod = oss.str();
                }
            | Term { $$ = $1;}
            | _addop Term {
                  if ($2.tipo != ENTERO && $2.tipo != REAL) //msgError(ERR_OPIZQ, $2.nlin, $2.ncol, $2.lexema);
                      msgErrorOperador(NUMERICO, $1.lexema, $1.nlin, $1.ncol, ERR_OPDER);
                  if (string($1.lexema) == "+") {
                      $$ = $2;
                  } else {
                      $$.cod = $2.cod;
                      $$.tipo = $2.tipo;
                      int tmp = NuevaTemporal();
                      ostringstream oss;
                      oss << $2.cod;
                      if ($2.tipo == ENTERO) {
                        oss << "mov #0 A;addop(-) Term(nint)\n";
                        oss << "subi " << $2.dir << '\n';
                        oss << "mov A " << tmp << '\n';
                      } else { //$2.tipo == REAL
                        oss << "mov $0 A;addop(-) Term(nfix)\n";
                        oss << "subr " << $2.dir << '\n';
                        oss << "mov A " << tmp << '\n';
                      }
                      //$$.tipo = ENTERO;
                      $$.dir = tmp;
                      $$.cod = oss.str();
                  }
                }
            ;

//Term      : Term mulop Factor
//Term      : Term ybool Factor
//Term      : Factor
Term        : Term _mulop Factor {
                ostringstream oss;
                oss << $1.cod << $3.cod;
                string mulop = minusculas($2.lexema);
                if ($1.tipo != REAL && $1.tipo != ENTERO)
                    msgErrorOperador(NUMERICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPIZQ);
                    //msgError(ERR_OPIZQ, $2.nlin, $2.ncol, $2.lexema);
                if ($3.tipo != REAL && $3.tipo != ENTERO)
                    msgErrorOperador(NUMERICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPDER);
                    //msgError(ERR_OPDER, $2.nlin, $2.ncol, $2.lexema);

                int tmp = NuevaTemporal();
                $$.dir = tmp;
                if (mulop == "*") {
                    if ($1.tipo == REAL || $3.tipo == REAL) {
                        $$.tipo = REAL;
                        if ($1.tipo == ENTERO) {//$3.tipo == REAL
                          oss << "mov "  << $1.dir << " A;Term(nint) mulop(*) Factor(nfix)\n";
                          oss << "itor\n";
                          oss << "mulr " << $3.dir << '\n';
                          oss << "mov A "<< tmp << '\n';
                        } else if ($3.tipo == ENTERO) { //$1.tipo == REAL
                          oss << "mov "  << $3.dir << " A;Term(nfix) mulop(*) Factor(nint)\n";
                          oss << "itor\n";
                          oss << "mulr " << $1.dir << '\n';
                          oss << "mov A "<< tmp << '\n';
                        } else { //$1.tipo == REAL && $3.tipo == REAL
                          oss << "mov "  << $1.dir << " A;Term(nfix) mulop(*) Factor(nfix)\n";
                          oss << "mulr " << $3.dir << '\n';
                          oss << "mov A "<< tmp << '\n';
                        }
                    } else { //$1.tipo == ENTERO && $3.tipo == ENTERO
                        $$.tipo = ENTERO;
                        oss << "mov "  << $1.dir << " A;Term(nint) mulop(*) Factor(nint)\n";
                        oss << "muli " << $3.dir << '\n';
                        oss << "mov A "<< tmp << '\n';
                    }
                } else if (mulop == "/") {
                    $$.tipo = REAL;
                    if ($1.tipo == ENTERO && $3.tipo == ENTERO) {
                        oss << "mov "  << $3.dir << " A;Term(nint) mulop(/) Factor(nint)\n";
                        oss << "itor\n";
                        oss << "mov A "<< tmp << '\n';
                        oss << "mov "  << $1.dir << " A\n";
                        oss << "itor\n";
                        oss << "divr " << tmp << '\n';
                        oss << "mov A "<< tmp << '\n';
                    } else if ($1.tipo == REAL && $3.tipo == REAL) {
                        oss << "mov "  << $1.dir << " A;Term(nfix) mulop(/) Factor(nfix)\n";
                        oss << "divr " << $3.dir << '\n';
                        oss << "mov A "<< tmp << '\n';
                    } else { //MIX real y entero
                        if ($1.tipo == ENTERO) { //$1.tipo == ENTERO && $3.tipo == REAL
                          oss << "mov "  << $1.dir << " A;Term(nint) mulop(/) Factor(nfix)\n";
                          oss << "itor\n";
                          oss << "divr " << $3.dir << '\n';
                          oss << "mov A "<< tmp << '\n';
                        } else {                //$1.tipo == REAL && $3.tipo == ENTERO
                          oss << "mov "  << $3.dir << " A;Term(nfix) mulop(/) Factor(nint)\n";
                          oss << "itor\n";
                          oss << "mov A "<< tmp << '\n';
                          oss << "mov "  << $1.dir << " A\n";
                          oss << "divr " << tmp << '\n';
                          oss << "mov A "<< tmp << '\n';
                        }
                    }
                } else if (mulop == "div") {
                    if ($1.tipo == ENTERO && $3.tipo == ENTERO) {
                        $$.tipo = ENTERO;
                        oss << "mov "  << $1.dir << " A;Term(nint) mulop(divi) Factor(nint)\n";
                        oss << "divi " << $3.dir << '\n';
                        oss << "mov A "<< tmp << '\n';
                    } else
                        msgError(ERR_CHR, $2.nlin, $2.ncol, $2.lexema);
                } else if (mulop == "mod") {
                    if ($1.tipo == ENTERO && $3.tipo == ENTERO){
                        $$.tipo = ENTERO;
                        oss << "mov "  << $1.dir << " A;Term(nint) mulop(divi) Factor(nint)\n";
                        oss << "modi " << $3.dir << '\n';
                        oss << "mov A "<< tmp << '\n';
                    } else
                        msgError(ERR_CHR, $2.nlin, $2.ncol, $2.lexema);
                } else {
                    cerr << "ERROR FATAL mulop!!!!!!!!\n";
                }
                $$.cod = oss.str();
              }
            | Term _ybool {
                    if ($1.tipo != LOGICO) //msgError(ERR_OPIZQ, $2.nlin, $2.ncol, $2.lexema);
                        msgErrorOperador(LOGICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPIZQ);
                } Factor {
                    if ($4.tipo != LOGICO) //msgError(ERR_OPDER, $2.nlin, $2.ncol, $2.lexema);
                        msgErrorOperador(LOGICO, $2.lexema, $2.nlin, $2.ncol, ERR_OPDER);

                    int tmp = NuevaTemporal();
                    ostringstream oss;
                    oss << $1.cod  << $4.cod;
                    oss << "mov "  << $1.dir << " A;Term(bool) ybool Factor(bool)\n";
                    oss << "andi " << $4.dir << '\n';
                    oss << "mov A "<< tmp << '\n';
                    $$.tipo = LOGICO;
                    $$.dir = tmp;
                    $$.cod = oss.str();
                }
            | Factor { $$ = $1;}
            ;

//Factor    : Ref
//Factor    : nint
//Factor    : nfix
//Factor    : ctechar
//Factor    : ctebool
//Factor    : nobool Factor
//Factor    : lpar Expr rpar
//Factor    : chr lpar Esimple rpar
//Factor    : ord lpar Esimple rpar
//Factor    : trunc lpar Esimple rpar
Factor      : Ref   {$$ = $1;}
            | _nint {
                  ostringstream oss;
                  $$.tipo = ENTERO;
                  int tmp = NuevaTemporal();
                  oss << "mov #" << $1.lexema <<' '<< tmp <<";entero(nint)\n";
                  $$.cod = oss.str();
                  $$.dir = tmp;
                  $$.lexema = $1.lexema;
              }
            | _nfix {
                  ostringstream oss;
                  $$.tipo = REAL;
                  int tmp = NuevaTemporal();
                  oss << "mov $" << $1.lexema <<' '<< tmp <<";real(nfix))\n";
                  $$.cod = oss.str();
                  $$.dir = tmp;
                  $$.lexema = $1.lexema;
              }
            | _ctechar {
                  ostringstream oss;
                  $$.tipo = CHAR;
                  int tmp = NuevaTemporal();
                  //TODO, cuidado aquí.
                  int letra = (int)$1.lexema[1];
                  if (letra == 92) {
                    int letra2 = (int)$1.lexema[2];
                    if (letra2 == 110)      // '\n'
                        letra = 10;
                    else if (letra2 == 48)  // '\0'
                        letra = 0;
                    else if (letra2 == 92)  // '\\'
                        letra = 92;
                  }
                  oss << "mov #" << letra <<' '<< tmp <<";char(ctechar)\n";
                  //cout << "mov #" << $1.lexema <<' '<< tmp <<";char(ctechar)\n";
                  $$.cod = oss.str();
                  $$.dir = tmp;
                  $$.lexema = $1.lexema;
              }
            | _ctebool {
                  ostringstream oss;
                  $$.tipo = LOGICO;
                  int tmp = NuevaTemporal();
                  oss << "mov #" << ctebool($1.lexema) <<' '<< tmp <<";logico(ctebool)\n";
                  $$.cod = oss.str();
                  $$.dir = tmp;
                  $$.lexema = $1.lexema;
              }
            | _nobool Factor {
                if ($2.tipo != LOGICO)
                  msgError(ERR_IFWHILE, nlin, ncol, $2.lexema);

                int tmp = NuevaTemporal();
                ostringstream oss;
                oss << $2.cod;
                oss << "mov "  << $2.dir << " A;nobool Factor\n";
                oss << "noti\n";
                oss << "mov A "<< tmp << '\n';
                $$.tipo = LOGICO;
                $$.dir = tmp;
                $$.cod = oss.str();
                $$.lexema = $2.lexema;
              }
            | _lpar Expr _rpar {$$ = $2;}
            | _chr _lpar Esimple _rpar{
                if ($3.tipo != ENTERO)
                  msgError(ERR_CHR, $1.nlin, $1.ncol, $1.lexema);
                int tmp = NuevaTemporal();
                ostringstream oss;
                oss << $3.cod;
                //oss << "mov " << $3.dir <<' '<< tmp <<";chr lpar Esimple(nint) rpar\n";
                oss << "mov " << $3.dir << " A;chr lpar Esimple(nint) rpar\n";
                oss << "modi #256\n";
                oss << "mov A "<< tmp << '\n';
                $$.tipo = CHAR;
                $$.lexema = $3.lexema;
                $$.dir = tmp;
                $$.cod = oss.str();
              }
            | _ord _lpar Esimple _rpar {
                  ostringstream oss;
                  oss << $3.cod;
                  int tmp = NuevaTemporal();
                  if ($3.tipo == ENTERO) {
                      oss << "mov " << $3.dir <<' '<< tmp <<";ord lpar Esimple(nint) rpar\n";
                  } else if ($3.tipo == CHAR) {
                      //oss << "mov " << $3.dir <<' '<< tmp <<";ord lpar Esimple(char) rpar\n";
                      oss << "mov " << $3.dir << " A;ord lpar Esimple(char) rpar\n";
                      oss << "modi #256\n";
                      oss << "mov A "<< tmp << '\n';
                  } else if ($3.tipo == LOGICO) {
                      //TODO
                      oss << "mov " << $3.dir <<' '<< tmp <<";ord lpar Esimple(nbool) rpar\n";
                  } else
                      msgError(ERR_ORD, $1.nlin, $1.ncol, $1.lexema);
                  $$.tipo = ENTERO;
                  $$.lexema = $3.lexema;
                  $$.dir = tmp;
                  $$.cod = oss.str();
              }
            | _trunc _lpar Esimple _rpar {
                  ostringstream oss;
                  int tmp = NuevaTemporal();
                  oss << $3.cod;
                  if ($3.tipo == ENTERO) {
                      //oss << "mov " << $3.dir <<' '<< tmp <<";ord lpar Esimple(nint) rpar\n";
                      oss << ";trunc lpar Esimple(nint) rpar\n";
                      $$.dir = $3.dir;
                  } else if ($3.tipo == REAL) {
                      oss << "mov " << $3.dir <<" A;trunc lpar Esimple(real) rpar\n";
                      oss << "rtoi\n";
                      oss << "mov A " << tmp <<"\n";
                      $$.dir = tmp;
                  } else
                      msgError(ERR_ORD, $3.nlin, $3.ncol, $3.lexema);
                  $$.tipo = ENTERO;
                  $$.lexema = $3.lexema;
                  $$.cod = oss.str();
              }
            ;

//Ref       : id
//Ref       : id lcor LisExpr rcor
Ref         : _id {
                simboloAux = tsimbolos.buscarSimbolo($1.lexema);
                //Error no declarado.
                if (simboloAux == NULL) {
                  msgError(ERR_NODECL, $1.nlin, $1.ncol, $1.lexema);
                }
                $$.tipo = simboloAux->tipo;
                $$.dir = simboloAux->dir;
                //$$.nombre = simboloAux.nombre;
                //$$.tam = simboloAux.tam;
              }
            | _id _lcor {
                  simboloAux = tsimbolos.buscarSimbolo($1.lexema);
                  if (simboloAux == NULL)
                    msgError(ERR_NODECL, $1.nlin, $1.ncol, $1.lexema);
                  $$.numElem = ttipos.numIndices(simboloAux->tipo);
              } LisExpr _rcor {
                  simboloAux = tsimbolos.buscarSimbolo($1.lexema);
                  if (simboloAux == NULL)
                    msgError(ERR_NODECL, $1.nlin, $1.ncol, $1.lexema);
                  if (simboloAux->tipo < 5)
                    msgError(ERR_SOBRAN, $1.nlin, $1.ncol, $1.lexema);
                  if ($4.tipo != ENTERO)
                    msgError(ERR_INDICE_ENTERO, $2.nlin, $2.ncol, $2.lexema);
                  if (ttipos.numIndices(simboloAux->tipo) > $4.numElem)
                    msgError(ERR_FALTAN, $5.nlin, $5.ncol, $5.lexema);
                    //cout << "$4.numElem: " << $4.numElem << endl;
                    //cout << "ttipos.numIndices(simboloAux->tipo): " << ttipos.numIndices(simboloAux->tipo) << endl;
                  if (ttipos.numIndices(simboloAux->tipo) < $4.numElem)
                    msgError(ERR_SOBRAN, $1.nlin, $1.ncol, $1.lexema);

                  $$.tipo = simboloAux->tipo;
                  $$.cod = $1.cod + $4.cod;
                  //lref.clear();
              }
            ;

//LisExpr   : LisExpr coma Expr
//LisExpr   : Expr
LisExpr     : LisExpr _coma Expr {
                  $$.numElem = $1.numElem + 1;
                  if ($$.numElem > $-1.numElem)
                      msgError(ERR_SOBRAN, $3.nlin, $3.ncol+strlen($3.lexema), $1.lexema);
                  if ($3.tipo != ENTERO)
                      msgError(ERR_INDICE_ENTERO, $-2.nlin, $-2.ncol, $-2.lexema);
                  $$.cod = $1.cod + $3.cod;
                  $$.tipo = $3.tipo;
                }
            | Expr {
                  $$.numElem = 1;
                  $$.cod = $1.cod;
                  $$.tipo = $1.tipo;
                  //No es necesario comprobar aquí si sobran índices, va implícito en "_id _lcor LisExpr _rcor".
                }
            ;

%%

void msgErrorSemantico(int nerror,int nlin,int ncol,const char *s){
    switch (nerror) {
        case 1: fprintf(stderr,"Error semantico (%d,%d): '%s' no ha sido declarado\n",nlin,ncol,s);
          break;
        case 2: fprintf(stderr,"Error semantico (%d,%d): '%s' debe ser mayor que cero\n",nlin,ncol,s);
          break;
        case 3: fprintf(stderr,"Error semantico (%d,%d): '%s' no es una variable\n",nlin,ncol,s);
          break;
        case 4: fprintf(stderr,"Error semantico (%d,%d): '%s' debe ser de tipo real\n",nlin,ncol,s);
          break;
        case 5: fprintf(stderr,"Error semantico (%d,%d): '%s' ya existe en este ámbito\n",nlin,ncol,s);
          break;
        //case 6: fprintf(stderr,"Error semantico (%d,%d): '%s' ya ha sido declarado\n",nlin,ncol,s);
        //  break;
    }
    exit(1);
}

void msgError(int nerror,int nlin,int ncol,const char *s) {
   switch (nerror) {
       case ERRLEXICO: fprintf(stderr,"Error lexico (%d,%d): caracter '%s' incorrecto\n",nlin,ncol,s);
          break;
       case ERRSINT: fprintf(stderr,"Error sintactico (%d,%d): en '%s'\n",nlin,ncol,s);
          break;
       case ERREOF: fprintf(stderr,"Error sintactico: fin de fichero inesperado\n");
          break;
       case ERRLEXEOF: fprintf(stderr,"Error lexico: fin de fichero inesperado\n");
          break;
       default:
          fprintf(stderr,"Error semantico (%d,%d): ", nlin,ncol);

          switch(nerror) {
             case ERR_YADECL: fprintf(stderr,"variable '%s' ya declarada\n",s);
               break;
             case ERR_NODECL: fprintf(stderr,"variable '%s' no declarada\n",s);
               break;
             case ERR_MAXVAR:fprintf(stderr,"en la variable '%s', hay demasiadas variables declaradas\n",s);
               break;
             case ERR_NOCABE:fprintf(stderr,"la variable '%s' ya no cabe en memoria\n",s);
               // fila,columna de ':'
               break;
             case ERR_MAXTEMP:fprintf(stderr,"no hay espacio para variables temporales\n");
               // fila,columna da igual
               break;
             case ERR_RANGO:fprintf(stderr,"el segundo valor debe ser mayor o igual que el primero.");
               // fila,columna del segundo número del rango
               break;
             case ERR_IFWHILE:fprintf(stderr,"la expresion del '%s' debe ser de tipo booleano",s);
               break;

             case ERR_CHR:fprintf(stderr,"el argumento de '%s' debe ser entero.",s);
               break;
             case ERR_ORD:fprintf(stderr,"el argumento de '%s' debe ser entero, caracter o booleano.",s);
               break;
             case ERR_TRUNC:fprintf(stderr,"el argumento de '%s' debe ser entero o real.",s);
               break;

             case ERR_FALTAN: fprintf(stderr,"faltan indices\n");
               // fila,columna del id (si no hay ningún í­ndice) o del último ']'
               break;
             case ERR_SOBRAN: fprintf(stderr,"sobran indices\n");
               // fila,columna del '[' si no es array, o de la ',' que sobra
               break;
             case ERR_INDICE_ENTERO: fprintf(stderr,"el indice de un array debe ser de tipo entero\n");
               // fila,columna del '[' si es el primero, o de la ',' inmediatamente anterior
               break;

             case ERR_ASIG: fprintf(stderr,"tipos incompatibles en la asignacion\n");
               // fila,columna del ':='
               break;
             case ERR_OPIZQ: fprintf(stderr,"el operando de la izquierda de %s\n",s);
               // fila,columna del operador
               break;
             case ERR_OPDER: fprintf(stderr,"el operando de la derecha de %s\n",s);
               // fila,columna del operador
               break;
          }
      }
   exit(1);
}

// función para dar mensajes de error de operadores aritméticos, relacionales y lógicos
//  tipoesp  :  tipo que se espera  (p.ej. LOGICO or LOGICO, CHAR < CHAR, NUMERICO + NUMERICO
//  op : operador (+,-,div,mod,<,or,and, ...)
//  lado : ERR_OPIZQ / ERR_OPDER
void msgErrorOperador(int tipoesp,const char *op,int linea,int columna,int lado) {
   string tipoEsp, mensaje;

   switch (tipoesp) {
     case ENTERO: tipoEsp="entero";
       break;
     case REAL: tipoEsp="real";
       break;
     case LOGICO: tipoEsp="logico";
       break;
     case CHAR: tipoEsp="caracter";
       break;
     case NUMERICO: tipoEsp="entero o real";
       break;
   }

   mensaje= "'" ;
   mensaje += op ;
   mensaje += "' debe ser de tipo " ;
   mensaje += tipoEsp ;
   msgError(lado,linea,columna,mensaje.c_str());
}

int ctebool(string s){
/*  for (int i=0; i<s.length(); i++)
      s[i] = tolower(s[i]);
  if (s == "false")
      return 0;
  else
      return 1;
*/
if (tolower(s[0]) == 't')
    return 1;
else
    return 0;
}

string minusculas(string s) {
  for (int i=0; i<s.length(); i++)
      s[i] = tolower(s[i]);
  return s;
}

string relop(string s) {
  for (int i=0; i<s.length(); i++)
      s[i] = tolower(s[i]);
  if (s == "=")
      return "eql";
  else if (s == "<>")
      return "neq";
  else if (s == "<")
      return "lss";
  else if (s == "<=")
      return "leq";
  else if (s == ">")
      return "gtr";
  else if (s == ">=")
      return "geq";
  else {
      cerr << "ERROR FATAL relop!!!!!!!!\n";
      return "halt;";
  }
}

void addRef(char *s){
    if(tsimbolos.existeID(s))
        lref.push_back(minusculas(s));
    else
        msgError(ERR_NODECL, nlin, ncol, s);
}

bool existeSimboloAux(string id){
  for (int i=0; i<id.length(); i++)
       id[i] = tolower(id[i]);
  for(int i=0; i < lsimbolos.size(); i++){
    if(lsimbolos[i] == id)
      return true;
  }
  return false;
}

void addSimbolo(char *s, int nlin, int ncol){
    if(tsimbolos.existeID(s) || existeSimboloAux(s))
        msgError(ERR_YADECL, nlin, ncol, s);
    else
        lsimbolos.push_back(s);
}

int yyerror(char *s) {
    extern int findefichero;  // de plp5.l
    if (findefichero)
       msgError(ERREOF,0,0,"");
    else
       msgError(ERRSINT,nlin,ncol-strlen(yytext),yytext);
}

int main(int argc,char *argv[]) {
    FILE *fent;
    if (argc==2) {
        fent = fopen(argv[1],"rt");
        if (fent) {
            yyin = fent;
            //Inicialización de las tablas de símbolos.
            inicializacion();
            //Llamada al análisis sintáctico.
            yyparse();
            fclose(fent);
        } else
            fprintf(stderr,"No puedo abrir el fichero\n");
    } else
        fprintf(stderr,"Uso: ejemplo <nombre de fichero>\n");
}

//Para probar que el léxico funciona correctamente:
/*
int main(int argc,char *argv[])
{
    FILE *fent;

    if (argc==2)
    {
        fent = fopen(argv[1],"rt");
        if (fent)
        {
            yyin = fent;
            while(yylex());
            fclose(fent);
        }
        else
            fprintf(stderr,"No puedo abrir el fichero\n");
    }
    else
        fprintf(stderr,"Uso: ejemplo <nombre de fichero>\n");
}
*/
