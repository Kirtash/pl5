/*------------------------------ ejemplo.l -------------------------------*/
D    [0-9]
L    [a-zA-Z]
LD   [0-9a-zA-Z]

%{
#include <string.h>
#include <string>
#include <iostream>

using namespace std;

#include "comun.h"
#include "plp5.tab.h"

int ncol = 1,
    nlin = 1;

int findefichero = 0;

int retID();
int retLowerID();
int ret(int token);
// función que actualiza 'nlin' y 'ncol' y devuelve el token

void msgError(int nerror, int nlin, int ncol, const char *s);
// función para producir mensajes de error

%}

%x COMENTARIO

%%
%{
/* codigo local */
%}

" "                {ncol++;}
[\t]               {ncol++;}
[\n]               {nlin++;ncol=1;}
"(*"                    {ncol += strlen(yytext); BEGIN(COMENTARIO);}
<COMENTARIO>[^(*\n]+    {ncol += strlen(yytext);}
<COMENTARIO>\n          {nlin++;ncol=1;}
<COMENTARIO><<EOF>>     {msgError(ERRLEXEOF,-1,-1,"");}
<COMENTARIO>"*)"        {ncol += strlen(yytext); BEGIN(INITIAL);}
<COMENTARIO>[*)]        {ncol += strlen(yytext);}

  /*
  // "("                {return ret(pari);}
  // ")"                {return ret(pard);}
  // "*"                {return ret(mulop);}
  // "/"                {return ret(mulop);}
  // "+"                {return ret(addop);}
  // "-"                {return ret(addop);}
  // ";"                {return ret(pyc);}
  // ","                {return ret(coma);}
  // "="                {return ret(asig);}
  // "{"                {return ret(llavei);}
  // "}"                {return ret(llaved);}
  // "["                {return ret(cori);}
  // "]"                {return ret(cord);}
  // "double"           {return ret(Double);}
  // "int"              {return ret(Int);}
  // "main"             {return ret(Main);}
  */

        /* Las palabras reservadas deben aparecer antes de la regla que
          reconoce los identificadores, para evitar que sean reconocidas
          como identificadores en lugar de como palabras reservadas */

(?i:"program")          {return ret(_program);}
(?i:"begin")            {return ret(_begin);}
(?i:"end")              {return ret(_end);}
(?i:"var")              {return ret(_var);}
(?i:"boolean")          {return ret(_boolean);}
(?i:"integer")          {return ret(_integer);}
(?i:"real")             {return ret(_real);}
(?i:"char")             {return ret(_char);}
(?i:"writeln")          {return ret(_wri);}
(?i:"write")            {return ret(_wri);}
(?i:"read")             {return ret(_read);}
(?i:"if")               {return ret(_if);}
(?i:"then")             {return ret(_then);}
(?i:"else")             {return ret(_else);}
(?i:"while")            {return ret(_while);}
(?i:"do")               {return ret(_do);}
(?i:"and")              {return ret(_ybool);}
(?i:"or")               {return ret(_obool);}
(?i:"div")              {return ret(_mulop);}
(?i:"mod")              {return ret(_mulop);}
(?i:"true")             {return ret(_ctebool);}
(?i:"false")            {return ret(_ctebool);}
(?i:"not")              {return ret(_nobool);}
(?i:"chr")              {return ret(_chr);}
(?i:"ord")              {return ret(_ord);}
(?i:"trunc")            {return ret(_trunc);}
(?i:"array")            {return ret(_array);}
(?i:"of")               {return ret(_of);}
        /*
        "program"          {return ret{program}}
        "begin"            {return ret{begin}}
        "end"              {return ret{end}}
        "var"              {return ret{var}}
        "boolean"          {return ret{boolean}}
        "integer"          {return ret{integer}}
        "real"             {return ret{real}}
        "char"             {return ret{char}}
        "writeln"          {return ret{wri}}
        "write"            {return ret{wri}}
        "read"             {return ret{read}}
        "if"               {return ret{if}}
        "then"             {return ret{then}}
        "else"             {return ret{else}}
        "while"            {return ret{while}}
        "do"               {return ret{do}}
        "and"              {return ret{ybool}}
        "or"               {return ret{obool}}
        "div"              {return ret{mulop}}
        "mod"              {return ret{mulop}}
        "true"             {return ret{ctebool}}
        "false"            {return ret{ctebool}}
        "not"              {return ret{nobool}}
        "chr"              {return ret{chr}}
        "ord"              {return ret{ord}}
        "trunc"            {return ret{trunc}}
        "array"            {return ret{array}}
        "of"               {return ret{of}}
        */
        /* Las palabras reservadas deben aparecer antes de la regla que
          reconoce los identificadores, para evitar que sean reconocidas
          como identificadores en lugar de como palabras reservadas */
{L}({LD})*         {return ret(_id);}
        /* {L}({LD})*         {return retID();} */
        /* {L}({LD})*         {return retLowerID();}*/
{D}+               {return ret(_nint);}
{D}+(\.){D}+       {return ret(_nfix);}

  /*  Cualquier carácter imprimible entre dos “’” */
"'"\\n"'"          {return ret(_ctechar);}
"'"\\0"'"          {return ret(_ctechar);}
"'"\\\\"'"         {return ret(_ctechar);}
"'"[\040-\377]"'"  {return ret(_ctechar);}

","                {return ret(_coma);}
";"                {return ret(_pyc);}
".."               {return ret(_ptopto);}
"."                {return ret(_punto);}
":="               {return ret(_assop);}
":"                {return ret(_dosp);}
"("                {return ret(_lpar);}
")"                {return ret(_rpar);}
"="                {return ret(_relop);}
"<>"               {return ret(_relop);}
"<="               {return ret(_relop);}
"<"                {return ret(_relop);}
">="               {return ret(_relop);}
">"                {return ret(_relop);}
"+"                {return ret(_addop);}
"-"                {return ret(_addop);}
"*"                {return ret(_mulop);}
"/"                {return ret(_mulop);}
"["                {return ret(_lcor);}
"]"                {return ret(_rcor);}

.                  {msgError(ERRLEXICO,nlin,ncol,yytext);}

        /*"eof"    {return ret(eof);}   //Implícito en la regla inicial de la gramática. */
        /*  Para comprobar que después de un programa correcto no aparecen más tokens en
            el fichero, es decir, que el el siguiente token es el del final del fichero,
            es necesario hacer una comprobación en la acción semántica situada al final
            de la regla: S → Sp main pari pard Bloque
            En dicha acción semántica se debe llamar directamente al analizador léxico:
            int token = yylex();
            Si el valor de token es 0, el fichero termina correctamente.
            Si es cualquier otro valor, hay que generar un error sintático con la
            llamada “yyerror("")”
        */
        /*"print"            {return ret(print);}*/
%%

//Importante, no tocar.
int yywrap(void) {findefichero=1; return 1;} /* para no tener que linkar con la libreria del lex */

bool coincide(const char* cadAux) {
  return !(strcasecmp(cadAux, yylval.lexema));
}

int retLowerID()
{
  yylval.lexema=strdup(yytext);
  yylval.nlin=nlin;
  yylval.ncol=ncol;
  int tam = strlen(yylval.lexema);
  ncol+=tam;
  for (int i=0; i<tam; i++)
      yylval.lexema[i] = tolower(yylval.lexema[i]);
  return _id;
}

int retID()
{
  yylval.lexema=strdup(yytext);
  yylval.nlin=nlin;
  yylval.ncol=ncol;
  ncol+=(strlen(yylval.lexema));

  if (coincide("program"))   return _program;
  if (coincide("begin"))     return _begin;
  if (coincide("end"))       return _end;
  if (coincide("var"))       return _var;
  if (coincide("boolean"))   return _boolean;
  if (coincide("integer"))   return _integer;
  if (coincide("real"))      return _real;
  if (coincide("char"))      return _char;
  if (coincide("writeln"))   return _wri;
  if (coincide("write"))     return _wri;
  if (coincide("read"))      return _read;
  if (coincide("if"))        return _if;
  if (coincide("then"))      return _then;
  if (coincide("else"))      return _else;
  if (coincide("while"))     return _while;
  if (coincide("do"))        return _do;
  if (coincide("and"))       return _ybool;
  if (coincide("or"))        return _obool;
  if (coincide("div"))       return _mulop;
  if (coincide("mod"))       return _mulop;
  if (coincide("true"))      return _ctebool;
  if (coincide("false"))     return _ctebool;
  if (coincide("not"))       return _nobool;
  if (coincide("chr"))       return _chr;
  if (coincide("ord"))       return _ord;
  if (coincide("trunc"))     return _trunc;
  if (coincide("array"))     return _array;
  if (coincide("of"))        return _of;
  //Default -> ID.
  return _id;
}

int ret(int token)
{
  yylval.lexema=strdup(yytext);
  yylval.nlin=nlin;
  yylval.ncol=ncol;
  ncol+=(strlen(yytext));
  return(token);
}
